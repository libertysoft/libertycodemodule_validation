LibertyCodeModule_Validation
============================



Description
-----------

Library contains framework modules, 
to manage application validation.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code_module/validation ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code_module/validation": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Application installation
------------------------

#### Configuration

1. Configuration: application module: "<project root path>/config/Module.<config_file_ext>"

    Add in list part, required modules:

    Example for YML configuration format, from composer installation:

    ```yml
    list: [
        {
            path: "/vendor/liberty_code_module/validation/src/validation",
            config_parser: {
                type: "string_table_php",
                source_format_get_regexp: "#^\\<\\?php\\s*(.*)(\\s\\?\\>)?\\s*$#ms",
                source_format_set_pattern: "<?php \\n%1$s",
                cache_parser_require: true,
                cache_file_parser_require: true
            }
        },
        {
            path: "/vendor/liberty_code_module/validation/src/rule",
            config_parser: {
                type: "string_table_php",
                source_format_get_regexp: "#^\\<\\?php\\s*(.*)(\\s\\?\\>)?\\s*$#ms",
                source_format_set_pattern: "<?php \\n%1$s",
                cache_parser_require: true,
                cache_file_parser_require: true
            }
        },
        {
            path: "/vendor/liberty_code_module/validation/src/cache",
            config_parser: {
                type: "string_table_php",
                source_format_get_regexp: "#^\\<\\?php\\s*(.*)(\\s\\?\\>)?\\s*$#ms",
                source_format_set_pattern: "<?php \\n%1$s",
                cache_parser_require: true,
                cache_file_parser_require: true
            }
        }
    ]
    ```

---



Configuration
-------------

#### Application parameters configuration

- Use following file on your modules to configure specific elements
    
    ```sh
    <module_root_path>/config/ParamApp.php
    ```

- Elements configurables

    - Configuration to param validator.
    
    - Configuration to param cache.

---



Usage
-----

Following examples consider command line application relative path as 
"bin/app".

#### Validation

Features allow to set and manage validation, available in application.

_Example_

```sh
cd "<project_root_path>"
...
// Get all rule names
php bin/app validation:rule:get:rule-name
```

#### rule

Features allow to set and manage rules, available for application validation.

#### Cache

Features allow to set and manage cache, available for application validation.


