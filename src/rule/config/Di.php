<?php

use liberty_code\validation\rule\standard\call\model\CallableRule;
use liberty_code\validation\rule\standard\type_string\model\TypeStringRule;
use liberty_code\validation\rule\standard\type_numeric\model\TypeNumericRule;
use liberty_code\validation\rule\standard\type_boolean\model\TypeBooleanRule;
use liberty_code\validation\rule\standard\type_array\model\TypeArrayRule;
use liberty_code\validation\rule\standard\type_date\model\TypeDateRule;
use liberty_code\validation\rule\standard\type_object\model\TypeObjectRule;
use liberty_code\validation\rule\standard\is_null\model\IsNullRule;
use liberty_code\validation\rule\standard\is_empty\model\IsEmptyRule;
use liberty_code\validation\rule\standard\is_callable\model\IsCallableRule;
use liberty_code\validation\rule\standard\compare_equal\model\CompareEqualRule;
use liberty_code\validation\rule\standard\compare_less\model\CompareLessRule;
use liberty_code\validation\rule\standard\compare_greater\model\CompareGreaterRule;
use liberty_code\validation\rule\standard\compare_between\model\CompareBetweenRule;
use liberty_code\validation\rule\standard\compare_in\model\CompareInRule;
use liberty_code\validation\rule\standard\string_start\model\StringStartRule;
use liberty_code\validation\rule\standard\string_end\model\StringEndRule;
use liberty_code\validation\rule\standard\string_contain\model\StringContainRule;
use liberty_code\validation\rule\standard\string_regexp\model\StringRegexpRule;
use liberty_code\validation\rule\standard\string_date_format\model\StringDateFormatRule;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\validation\validator\rule\sub_rule\not\model\NotSubRule;
use liberty_code\validation\validator\rule\sub_rule\size\model\SizeSubRule;
use liberty_code\validation\validator\rule\sub_rule\iterate\model\IterateSubRule;
use liberty_code\validation\validator\rule\group_sub_rule\group_and\model\AndGroupSubRule;
use liberty_code\validation\validator\rule\group_sub_rule\group_or\model\OrGroupSubRule;
use liberty_code\model\validation\rule\entity_collection\attribute_exist\model\AttrExistEntityCollectionRule;
use liberty_code\model\validation\rule\validation_entity\model\ValidEntityRule;
use liberty_code\model\validation\rule\validation_entity_collection\model\ValidEntityCollectionRule;
use liberty_code\model\validation\rule\new_entity\model\NewEntityRule;
use liberty_code\model\validation\rule\new_entity_collection\model\NewEntityCollectionRule;
use liberty_code\sql\validation\rule\sql_exist\model\ExistSqlRule;



return array(
    // Rule services
    // ******************************************************************************

    'validation_rule_callable' => [
        'source' => CallableRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_type_string' => [
        'source' => TypeStringRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_type_numeric' => [
        'source' => TypeNumericRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_type_boolean' => [
        'source' => TypeBooleanRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_type_array' => [
        'source' => TypeArrayRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_type_date' => [
        'source' => TypeDateRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_type_object' => [
        'source' => TypeObjectRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_is_null' => [
        'source' => IsNullRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_is_empty' => [
        'source' => IsEmptyRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_is_callable' => [
        'source' => IsCallableRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_compare_equal' => [
        'source' => CompareEqualRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_compare_less' => [
        'source' => CompareLessRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_compare_greater' => [
        'source' => CompareGreaterRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_compare_between' => [
        'source' => CompareBetweenRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_compare_in' => [
        'source' => CompareInRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_string_start' => [
        'source' => StringStartRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_string_end' => [
        'source' => StringEndRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_string_contain' => [
        'source' => StringContainRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_string_regexp' => [
        'source' => StringRegexpRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_string_date_format' => [
        'source' => StringDateFormatRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_sub_rule_not' => [
        'source' => NotSubRule::class,
        'argument' => [
            ['type' => 'class', 'value' => ValidatorInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_sub_rule_size' => [
        'source' => SizeSubRule::class,
        'argument' => [
            ['type' => 'class', 'value' => ValidatorInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_sub_rule_iterate' => [
        'source' => IterateSubRule::class,
        'argument' => [
            ['type' => 'class', 'value' => ValidatorInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_group_sub_rule_and' => [
        'source' => AndGroupSubRule::class,
        'argument' => [
            ['type' => 'class', 'value' => ValidatorInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_group_sub_rule_or' => [
        'source' => OrGroupSubRule::class,
        'argument' => [
            ['type' => 'class', 'value' => ValidatorInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_entity_collection_attribute_exist' => [
        'source' => AttrExistEntityCollectionRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_validation_entity' => [
        'source' => ValidEntityRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_validation_entity_collection' => [
        'source' => ValidEntityCollectionRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_new_entity' => [
        'source' => NewEntityRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_new_entity_collection' => [
        'source' => NewEntityCollectionRule::class,
        'option' => [
            'shared' => true
        ]
    ],

    'validation_rule_sql_exist' => [
        'source' => ExistSqlRule::class,
        'argument' => [],
        'option' => [
            'shared' => true
        ]
    ]
);