<?php

use liberty_code_module\validation\rule\boot\RuleBootstrap;



return array(
    'validation_rule_bootstrap' => [
        'call' => [
            'class_path_pattern' => RuleBootstrap::class . ':boot'
        ]
    ]
);