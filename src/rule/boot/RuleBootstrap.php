<?php
/**
 * Description :
 * This class allows to define rule module bootstrap class.
 * Rule module bootstrap allows to boot rule module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\validation\rule\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\validation\rule\api\RuleCollectionInterface;
use liberty_code\validation\rule\standard\call\model\CallableRule;
use liberty_code\validation\rule\standard\type_string\model\TypeStringRule;
use liberty_code\validation\rule\standard\type_numeric\model\TypeNumericRule;
use liberty_code\validation\rule\standard\type_boolean\model\TypeBooleanRule;
use liberty_code\validation\rule\standard\type_array\model\TypeArrayRule;
use liberty_code\validation\rule\standard\type_date\model\TypeDateRule;
use liberty_code\validation\rule\standard\type_object\model\TypeObjectRule;
use liberty_code\validation\rule\standard\is_null\model\IsNullRule;
use liberty_code\validation\rule\standard\is_empty\model\IsEmptyRule;
use liberty_code\validation\rule\standard\is_callable\model\IsCallableRule;
use liberty_code\validation\rule\standard\compare_equal\model\CompareEqualRule;
use liberty_code\validation\rule\standard\compare_less\model\CompareLessRule;
use liberty_code\validation\rule\standard\compare_greater\model\CompareGreaterRule;
use liberty_code\validation\rule\standard\compare_between\model\CompareBetweenRule;
use liberty_code\validation\rule\standard\compare_in\model\CompareInRule;
use liberty_code\validation\rule\standard\string_start\model\StringStartRule;
use liberty_code\validation\rule\standard\string_end\model\StringEndRule;
use liberty_code\validation\rule\standard\string_contain\model\StringContainRule;
use liberty_code\validation\rule\standard\string_regexp\model\StringRegexpRule;
use liberty_code\validation\rule\standard\string_date_format\model\StringDateFormatRule;
use liberty_code\validation\validator\rule\sub_rule\not\model\NotSubRule;
use liberty_code\validation\validator\rule\sub_rule\size\model\SizeSubRule;
use liberty_code\validation\validator\rule\sub_rule\iterate\model\IterateSubRule;
use liberty_code\validation\validator\rule\group_sub_rule\group_and\model\AndGroupSubRule;
use liberty_code\validation\validator\rule\group_sub_rule\group_or\model\OrGroupSubRule;
use liberty_code\model\validation\rule\entity_collection\attribute_exist\model\AttrExistEntityCollectionRule;
use liberty_code\model\validation\rule\validation_entity\model\ValidEntityRule;
use liberty_code\model\validation\rule\validation_entity_collection\model\ValidEntityCollectionRule;
use liberty_code\model\validation\rule\new_entity\model\NewEntityRule;
use liberty_code\model\validation\rule\new_entity_collection\model\NewEntityCollectionRule;
use liberty_code\sql\validation\rule\sql_exist\model\ExistSqlRule;
use liberty_code_module\validation\rule\library\ConstRule;



class RuleBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Provider instance.
     * @var ProviderInterface
     */
    protected $objProvider;



    /**
     * DI: Rule collection instance.
     * @var RuleCollectionInterface
     */
    protected $objRuleCollection;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ProviderInterface $objProvider
     * @param RuleCollectionInterface $objRuleCollection
     */
    public function __construct(
        AppInterface $objApp,
        ProviderInterface $objProvider,
        RuleCollectionInterface $objRuleCollection
    )
    {
        // Init properties
        $this->objProvider = $objProvider;
        $this->objRuleCollection = $objRuleCollection;

        // Call parent constructor
        parent::__construct($objApp, ConstRule::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Get rules
        $tabRule = array_map(
            function($strClassPath)
            {
                return $this->objProvider->getFromClassPath($strClassPath);
            },
            array(
                CallableRule::class,
                TypeStringRule::class,
                TypeNumericRule::class,
                TypeBooleanRule::class,
                TypeArrayRule::class,
                TypeDateRule::class,
                TypeObjectRule::class,
                IsNullRule::class,
                IsEmptyRule::class,
                IsCallableRule::class,
                CompareEqualRule::class,
                CompareLessRule::class,
                CompareGreaterRule::class,
                CompareBetweenRule::class,
                CompareInRule::class,
                StringStartRule::class,
                StringEndRule::class,
                StringContainRule::class,
                StringRegexpRule::class,
                StringDateFormatRule::class,
                NotSubRule::class,
                SizeSubRule::class,
                IterateSubRule::class,
                AndGroupSubRule::class,
                OrGroupSubRule::class,
                AttrExistEntityCollectionRule::class,
                ValidEntityRule::class,
                ValidEntityCollectionRule::class,
                NewEntityRule::class,
                NewEntityCollectionRule::class,
                ExistSqlRule::class
            )
        );

        // Hydrate rule collection
        $this->objRuleCollection->setTabRule($tabRule);
    }



}