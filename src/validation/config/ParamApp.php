<?php

use liberty_code\validation\validator\standard\model\StandardValidator;



return array(
    // Validation configuration
    // ******************************************************************************

    'validation' => [
        'validator' => [
            /**
             * Validator configuration format:
             * @see StandardValidator configuration format.
             */
            'config' => []
        ]
    ]
);