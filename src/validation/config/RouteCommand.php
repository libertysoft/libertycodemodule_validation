<?php

use liberty_code_module\validation\validation\rule\controller\command\RuleController;



return array(
    // Rule services
    // ******************************************************************************

    'validation_rule_get_rule_name' => [
        'source' => 'validation:rule:get:rule-name',
        'call' => [
            'class_path_pattern' => RuleController::class . ':actionGetRuleName'
        ],
        'description' => 'Get all rule names'
    ]
);