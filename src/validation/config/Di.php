<?php

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\validation\rule\api\RuleCollectionInterface;
use liberty_code\validation\rule\model\DefaultRuleCollection;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\validation\validator\standard\model\StandardValidator;
use liberty_code\validation\validation\factory\api\ValidationFactoryInterface;
use liberty_code\validation\validation\factory\standard\model\StandardValidationFactory;



return array(
    // Rule services
    // ******************************************************************************

    'validation_rule_collection' => [
        'source' => DefaultRuleCollection::class,
        'option' => [
            'shared' => true
        ]
    ],



    // Validator services
    // ******************************************************************************

    'validation_validator' => [
        'source' => StandardValidator::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'validation_rule_collection'],
            ['type' => 'config', 'value' => 'validation/validator/config'],
            ['type' => 'dependency', 'value' => 'validation_validator_cache_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Validation services
    // ******************************************************************************

    'validation_factory' => [
        'source' => StandardValidationFactory::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'validation_validator'],
            ['type' => 'mixed', 'value' => null],
            ['type' => 'class', 'value' => ProviderInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Preferences
    // ******************************************************************************

    RuleCollectionInterface::class => [
        'set' => ['type' => 'dependency', 'value' => 'validation_rule_collection'],
        'option' => [
            'shared' => true,
        ]
    ],

    ValidatorInterface::class => [
        'set' => ['type' => 'dependency', 'value' => 'validation_validator'],
        'option' => [
            'shared' => true,
        ]
    ],

    ValidationFactoryInterface::class => [
        'set' => ['type' => 'dependency', 'value' => 'validation_factory'],
        'option' => [
            'shared' => true,
        ]
    ]
);