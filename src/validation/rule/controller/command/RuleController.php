<?php
/**
 * Description :
 * This class allows to define rule controller class for command line.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\validation\validation\rule\controller\command;

use liberty_code\controller\controller\model\DefaultController;

use liberty_code\request_flow\response\model\DefaultResponse;
use liberty_code\validation\rule\api\RuleCollectionInterface;



class RuleController extends DefaultController
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Rule collection instance.
     * @var RuleCollectionInterface
     */
    protected $objRuleCollection;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RuleCollectionInterface $objRuleCollection
     */
    public function __construct(
        RuleCollectionInterface $objRuleCollection
    )
    {
        // Init properties
        $this->objRuleCollection = $objRuleCollection;

        // Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function checkAccessMethodEngine($strMethodNm, array $tabArg = null)
    {
        // Return result
        return true;
    }





    // Methods action
    // ******************************************************************************

    /**
     * Action to get rule names.
     *
     * @return DefaultResponse
     */
    public function actionGetRuleName()
    {
        // Init var
        $tabRuleNm = $this->objRuleCollection->getTabKey();

        // Get render
        $strRender = '';
        foreach($tabRuleNm as $strRuleNm)
        {
            $strRender .=
                ((trim($strRender) != '') ? PHP_EOL : '') .
                $strRuleNm;
        }

        // Get response
        $objResponse = new DefaultResponse();
        $objResponse->setContent($strRender . PHP_EOL);

        // Return response
        return $objResponse;
    }



}