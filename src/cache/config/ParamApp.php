<?php

use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\cache\repository\format\model\FormatRepository;



return array(
    // Validation configuration
    // ******************************************************************************

    'validation' => [
        // Cache configuration
        'cache' => [
            'register' => [
                /**
                 * Configuration array format:
                 * @see DefaultTableRegister configuration format.
                 */
                'config' => []
            ],

            /**
             * Configuration format:
             * @see FormatRepository configuration format.
             */
            'config' => [
                'key_pattern' => 'validation-%s',
                'key_regexp_select' => '#validation\-(.+)#'
            ]
        ],

        'validator' => [
            // Validator cache configuration
            'cache' => [
                /**
                 * Configuration format:
                 * @see FormatRepository configuration format.
                 */
                'config' => [
                    'key_pattern' => 'validation-validator-%s',
                    'key_regexp_select' => '#validation\-validator\-(.+)#'
                ]
            ]
        ]
    ]
);