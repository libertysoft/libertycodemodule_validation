<?php

use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\cache\format\model\FormatData;
use liberty_code\cache\repository\format\model\FormatRepository;



return array(
    // Cache services
    // ******************************************************************************

    'validation_cache_register' => [
        'source' => DefaultTableRegister::class,
        'argument' => [
            ['type' => 'config', 'value' => 'validation/cache/register/config'],
            ['type' => 'mixed', 'value' => null]
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'validation_cache_repository' => [
        'source' => FormatRepository::class,
        'argument' => [
            ['type' => 'config', 'value' => 'validation/cache/config'],
            ['type' => 'dependency', 'value' => 'validation_cache_register'],
            ['type' => 'class', 'value' => FormatData::class],
            ['type' => 'class', 'value' => FormatData::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Validator cache services
    // ******************************************************************************

    'validation_validator_cache_register' => [
        'source' => DefaultTableRegister::class,
        'set' => ['type' => 'dependency', 'value' => 'validation_cache_register'],
        'option' => [
            'shared' => true
        ]
    ],

    'validation_validator_cache_repository' => [
        'source' => FormatRepository::class,
        'argument' => [
            ['type' => 'config', 'value' => 'validation/validator/cache/config'],
            ['type' => 'dependency', 'value' => 'validation_validator_cache_register'],
            ['type' => 'class', 'value' => FormatData::class],
            ['type' => 'class', 'value' => FormatData::class]
        ],
        'option' => [
            'shared' => true
        ]
    ]
);